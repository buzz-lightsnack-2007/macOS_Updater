# The macOS Updater
**Install macOS updates from the TUI.**

Have you ever wanted to install macOS updates from the command-line, without having to worry too much about syntax? **This is the right place.** 

## Programs
### `default.py`
This one contains everything. It's a work-in-progress, so it is far from complete. 

### `betaEnrollment.py`
betaEnrollment.py allows you to get beta macOS updates, without having to download anything from Apple at all! The thing is, the installer packages that Apple provides only run some Terminal commands which this script has also unpacked. 

## Contributing
Do you wish to contribute? Sure, yes you can! You can create new issues via the issue tracker or create a fork and then do a pull request. 
