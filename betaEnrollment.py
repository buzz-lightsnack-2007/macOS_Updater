#! /usr/local/bin/python3

""" betaEnrollment
	install beta macOS software
"""


# Import modules.
import os; import time as clock

# on Terminal outputs
class screen:
	class formatting:
		# text formatting
		clear = '\033[0m'
		class color:
			purple = '\033[95m'
			cyan = '\033[96m'
			cyan_dark = '\033[36m'
			blue = '\033[94m'
			green = '\033[92m'
			yellow = '\033[93m'
			red = '\033[91m'
		class font:
			bold = '\033[1m'
			underline = '\033[4m'

	# screen functions
	def cls(times = 1): # to clear screen
		times = int(abs(times));
		for _ in range(times):
			try:
				clear
			except NameError:
				import os; return(os.system('clear'))

	class info: ### screen messages
		def status(message): print(formatting.font.bold + 'ℹ️\t' + formatting.clear + message)
		def err(message): print(formatting.font.bold + formatting.color.red + '❌\t' + formatting.clear + formatting.clear + message)
		def warning(message): print(formatting.font.bold + formatting.color.yellow + '⚠️\t' + formatting.clear + formatting.clear + message)
		def success(message): print(formatting.font.bold + formatting.color.green + '✅\t' + formatting.clear + formatting.clear + message)


class user: 
    result = ''
    betaRequest = None

class execution:
	command = None
	output = None

class programs:
	def displayCurrent():
		# display current seeding information, and at the same time, check if the user is an administrator
		
		if (os.system('sudo /System/Library/PrivateFrameworks/Seeding.framework/Versions/A/Resources/seedutil current')):
			# If this is true, then the user is not an administrator, or it was cancelled. The warning alone is good enough, we could safely cancel the operation.
			exit()
			return (False)
		else:
			print('')
			return (True)

	def enrollBeta(betaType):
		# import modules
		import os
		
		# Enroll to the beta.
		execution.command = 'sudo /System/Library/PrivateFrameworks/Seeding.framework/Versions/A/Resources/seedutil '

		if not(betaType):
			execution.command += 'unenroll'
		else:
			execution.command += 'enroll '
			
			if betaType == 1:
				execution.command += 'PublicSeed'
			elif betaType == 2:
				execution.command += 'DeveloperSeed'
		
		execution.output = os.system(execution.command)
		
		return(execution.output)

	def viewLisenceAgreement(betaType):
		if (betaType):
			# import the module
			import os
			import time as clock
			
			# Remove the wrong lisence agreement if it still exists.
			if (os.path.exists('/tmp/Beta Lisence Agreement.pdf')):
				os.remove('/tmp/Beta Lisence Agreement.pdf')
			
			# Pre-load the curl command.
			execution.command = 'curl -s '
			
			# Download and show the lisence agreement.
			if betaType == 1:
				execution.command += 'https://beta.apple.com/agreements/Beta_20200622.pdf -o /tmp/Beta\ Lisence\ Agreement.pdf '
			elif betaType == 2:
				execution.command += 'https://developer.apple.com/support/downloads/terms/apple-developer-program/Apple-Developer-Program-License-Agreement-20220606-English.pdf -o /tmp/Beta\ Lisence\ Agreement.pdf '
			
			# Download the file.
			execution.output = os.system(execution.command)
			
			# Open the file.
			if not(execution.output):
				os.system('open /tmp/Beta\ Lisence\ Agreement.pdf')
				print('\n' + screen.formatting.font.bold + 'Please carefully read the lisence agreement.' + screen.formatting.clear + ' [⌃C: cancel | ⏎: agree]')
				clock.sleep(10)
			
			# Prompt whether or not the user agrees with it.
			if not(execution.output):
				print('Do you agree with the terms and conditions?')
				try:
					input('').strip()
				except KeyboardInterrupt:
					return(False)
				else:
					return(True)
		else:
			return (True)

	def promptChange():
		# import modules
		import os
		import time as clock
		
		print(screen.formatting.font.bold + 'Would you want to use the public beta (1), private beta (2), or unenroll (0)? ' + screen.formatting.clear)
		clock.sleep(0.1)
		
		try:
			# Request for user input.
			user.result = abs(int(input('Please type your numerical choice to continue: ')))
			user.betaRequest = user.result
			
			if (user.result > 2):
				# Keep looping until the user does it right
				os.system('clear')
				return(False)
			else:
				if (programs.viewLisenceAgreement(user.betaRequest)):
					return(not(bool(programs.enrollBeta(user.betaRequest))))
					
		except KeyboardInterrupt:
			exit()

def main():
	while True:
		# import modules
		import time as clock
		import os
		
		# Run the programs.
		programs.displayCurrent()
		
		if (programs.promptChange()):
			exit()


main()

